from models import app, db, Country, City, Points_Of_Interest
from geopy.geocoders import Nominatim
import json

# need to import models @max


def create_countries():
    # code, currency, name, wiki
    f = open('JSON/countries.json', 'r')
    db_json = json.load(f)
    data = db_json
    for d in data:
        name, currency, code, wiki, wikiID = d["name"], d["currencyCodes"][0], d[
            "code"], f"http://www.wikidata.org/entity/{d['wikiDataId']}", d['wikiDataId']

        newCountry = Country(id=wikiID, country_name=name,
                             currency=currency, country_code=code, wiki=wiki)
        db.session.add(newCountry)
        db.session.commit()
        # break
        #print({"name": name, "currency": currency, "code": code, "wiki": wiki})
    return


def create_cities():
    # name, country, region, regioncode, countryCode, longitude, latitude
    f = open('JSON/cities.json', 'r')
    db_json = json.load(f)
    data = db_json
    for d in data:
        id, name, country, country_code, region, longitude, latitude = d["id"], d["name"], d[
            "country"], d["countryCode"], d["region"], d["longitude"], d["latitude"]

        newCity = City(id=id, city_name=name, country_name=country, country_code=country_code, region=region,
                       longitude=longitude, latitude=latitude)
        db.session.add(newCity)
        db.session.commit()

        # break
    # print({"name": name, "country": country, "country_code": country_code,
        # "region": region, "longitude": longitude, "latitude": latitude})
    return


def create_attractions():
    # city, longitude, latitude, name, category, tags
    f = open('JSON/pois.json', 'r')
    db_json = json.load(f)
    data = db_json
    for d in data:
        id = d["id"]
        location = str(d["geoCode"]["latitude"]) + ", " + str(
            d["geoCode"]["longitude"])
        geoLoc = Nominatim(user_agent="GetLoc")
        locname = geoLoc.reverse(location)

        city, longitude, latitude, name, category, tags = locname.raw["address"]["city"], d[
            "geoCode"]["longitude"], d["geoCode"]["latitude"], d["name"], d["category"], ", ".join(d["tags"][:6])

        newPOI = Points_Of_Interest(id=id, city_name=city, longitude=longitude, latitude=latitude,
                                    attraction_name=name, category=category, tags=tags)
        db.session.add(newPOI)
        db.session.commit()

        # break
    # print({"city": city, "longitude": longitude, "latitude": latitude,
    #       "name": name, "category": category, "tags": tags})
    return


create_countries()
create_cities()
create_attractions()

