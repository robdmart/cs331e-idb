import os 
import sys
import unittest
from models import db, Country, City, Points_Of_Interest

class Test(unittest.TestCase):

    #test inserts

    #Countries - name, currency, code, wiki
    def test_Country_insert(self):
        #make country object, add and commit
        c = Country(country_name = "United States of America", currency = "USD", country_code = "US", id = "Q30", wiki = "http://www.wikidata.org/entity/Q30")
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(Country).filter_by(country_name = "United States of America").one()
        self.assertEqual(str(q.country_name), "United States of America" )

        #delete the insert and commit
        db.session.query(Country).filter_by(country_name = "United States of America").delete()
        db.session.commit()
        
    def test_Country_insert1(self):
        #make country object, add and commit
        c = Country(country_name = "Spain", currency = "EUR", country_code = "ES", id = "Q29", wiki = "http://www.wikidata.org/entity/Q29")
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(Country).filter_by(country_name = "Spain").one()
        self.assertEqual(str(q.country_name), "Spain" )

        #delete the insert and commit
        db.session.query(Country).filter_by(country_name = "Spain").delete()
        db.session.commit()
        
    def test_Country_insert2(self):
        #make country object, add and commit
        c = Country(country_name = "Panama", currency = "PAB", country_code = "PA", id = "Q804")
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(Country).filter_by(country_name = "Panama").one()
        self.assertEqual(str(q.country_name), "Panama" )

        #delete the insert and commit
        db.session.query(Country).filter_by(country_name = "Panama").delete()
        db.session.commit()
        
    
    """
    Cities has 7 attributes city_name, country_name, countryCode, region, longitude, latitude
    """
    def test_City_insert(self):
        cc = Country(country_name = "France", currency = "EUR", country_code = "FR", id = "Q142", wiki = "http://www.wikidata.org/entity/Q142")
        db.session.add(cc)
        db.session.commit()
        
        #make city object, add and commit
        c = City(city_name = "Paris", country_name = "France", country_code = "FR", region = "╬le-de-France", longitude = 2.351388888, latitude = 48.856944444)
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(City).filter_by(city_name = "Paris").one()
        self.assertEqual(str(q.city_name), "Paris" )

        #delete the insert and commit
        db.session.query(City).filter_by(city_name = "Paris").delete()
        db.session.commit()

        db.session.query(Country).filter_by(country_name = "France").delete()
        db.session.commit()        
           
    def test_City_insert1(self):
        cc = Country(country_name = "Spain", currency = "EUR", country_code = "ES", id = "Q29", wiki = "http://www.wikidata.org/entity/Q29")
        db.session.add(cc)
        db.session.commit()
        
        #make city object, add and commit
        c = City(city_name = "Madrid", country_name = "Spain", country_code = "ES", region = "Community of Madrid", longitude = -3.691944444, latitude = 40.418888888)
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(City).filter_by(city_name = "Madrid").one()
        self.assertEqual(str(q.city_name), "Madrid" )

        #delete the insert and commit
        db.session.query(City).filter_by(city_name = "Madrid").delete()
        db.session.commit()
        
        db.session.query(Country).filter_by(country_name = "Spain").delete()
        db.session.commit()
    
    def test_City_insert2(self):
        cc = Country(country_name = "Spain", currency = "EUR", country_code = "ES", id = "Q29", wiki = "http://www.wikidata.org/entity/Q29")
        db.session.add(cc)
        db.session.commit()
        
        #make city object, add and commit
        c = City(city_name = "Las Palmas", country_name = "Spain", country_code = "ES", region = "Canary Islands", longitude = -14.333333333, latitude = 28.333333333)
        db.session.add(c)
        db.session.commit()

        #query to look up object, assert it matched initial input
        q = db.session.query(City).filter_by(city_name = "Las Palmas").one()
        self.assertEqual(str(q.city_name), "Las Palmas" )

        #delete the insert and commit
        db.session.query(City).filter_by(city_name = "Las Palmas").delete()
        db.session.commit()

        db.session.query(Country).filter_by(country_name = "Spain").delete()
        db.session.commit()
        


    # city, longitude, latitude, name, category, tags
    def test_POIS_insert(self):
        cc = Country(country_name = "country")
        db.session.add(cc)
        db.session.commit() 
        
        c = City(city_name = "city", country_name = "country")
        db.session.add(c)
        db.session.commit() 
         
        p = Points_Of_Interest(id = '0', city_name = "city", longitude = 4.225, latitude = -3.005, attraction_name = "attraction", category = "category", tags = "tags")
        db.session.add(p)
        db.session.commit()

        q = db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction").one()
        self.assertEqual( str(q.attraction_name), "attraction" )

        db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction").delete()
        db.session.commit()

        db.session.query(City).filter_by(city_name = "city").delete()
        db.session.commit()

        db.session.query(Country).filter_by(country_name = "country").delete()
        db.session.commit()
      
    def test_POIS_insert1(self):
        cc = Country(country_name = "country1")
        db.session.add(cc)
        db.session.commit() 
        
        c = City(city_name = "city1", country_name = "country1")
        db.session.add(c)
        db.session.commit() 
         
        p = Points_Of_Interest(id = '1', city_name = "city1", longitude = 4.115, latitude = -3.605, attraction_name = "attraction1", category = "category", tags = "tags")
        db.session.add(p)
        db.session.commit()

        q = db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction1").one()
        self.assertEqual( str(q.attraction_name), "attraction1" )

        db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction1").delete()
        db.session.commit()

        db.session.query(City).filter_by(city_name = "city1").delete()
        db.session.commit()

        db.session.query(Country).filter_by(country_name = "country1").delete()
        db.session.commit()
        
    def test_POIS_insert2(self):
        cc = Country(country_name = "country2")
        db.session.add(cc)
        db.session.commit() 
        
        c = City(city_name = "city2", country_name = "country2")
        db.session.add(c)
        db.session.commit() 
         
        p = Points_Of_Interest(id = '2', city_name = "city2", longitude = 5.778, latitude = -3.999, attraction_name = "attraction2", category = "category2", tags = "tags2")
        db.session.add(p)
        db.session.commit()

        q = db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction2").one()
        self.assertEqual( str(q.attraction_name), "attraction2" )

        db.session.query(Points_Of_Interest).filter_by(attraction_name = "attraction2").delete()
        db.session.commit()

        db.session.query(City).filter_by(city_name = "city2").delete()
        db.session.commit()

        db.session.query(Country).filter_by(country_name = "country2").delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
