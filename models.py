from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os

# username = user , password = password. set up on GCP

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgresql://postgres:dai101@localhost:5432/traveldb')
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgresql://postgres:robertmartinez@localhost:5432/traveldb')
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://ueyibgud:JpV93BnoK21jeI2OwJbADZP76_dMFrQf@queenie.db.elephantsql.com:5432/ueyibgud"
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class Country(db.Model):
    """
    Country has 7 attributes right now. 
    """
    __tablename__ = 'country'
    #id = db.Column(db.Integer, autoincrement=True)
    id = db.Column(db.String(16), nullable=True)
    country_name = db.Column(db.String(80), nullable=False, primary_key=True)
    currency = db.Column(db.String(10), nullable=True)
    country_code = db.Column(db.String(10), nullable=True)
    wiki = db.Column(db.String(80), nullable=True)
    img = db.Column(db.String(80), nullable=True)

    cities = db.relationship('City', backref='country')

    """
    setup relationships with cities and points of interest
    something like
    city = db.relationship('Cities', backref = 'city_name')
    do the same for cities and points of interest

    """

    def serialize(self):
        return{
            country_name: self.name,
            currency: self.currency,
            country_code: self.code,
            #wiki : self.wiki
        }


class City(db.Model):
    """
    Cities has 9 attributes img, name, country, region, regioncode, countryCode, longitude, latitude
    """
    __tablename__ = 'city'
    id = db.Column(db.Integer, nullable=True)
    city_name = db.Column(db.String(80), nullable=False, primary_key=True)
    country_name = db.Column(db.String(40), db.ForeignKey(
        'country.country_name'), nullable=False)
    country_code = db.Column(db.String(10), nullable=True)
    region = db.Column(db.String(80), nullable=True)
    longitude = db.Column(db.Float, nullable=True)
    latitude = db.Column(db.Float, nullable=True)
    img = db.Column(db.String(80), nullable=True)

    attractions = db.relationship('Points_Of_Interest', backref='city')
    #country_id = db.Column(db.String(16), db.ForeignKey('country.id'))

    def serialize(self):
        return{
            city_name: self.name,
            country_name: self.country,
            region: self.region,
            country_code: self.country_code,
            longitude: self.longitude,
            latitude: self.latitude
        }

    """
    setup relationships with countries and points of interest
    """


class Points_Of_Interest(db.Model):
    """
    Points_Of_Interest has 8 attributes img, tags, attraction_name, city_name, category, longitude, latitude
    """
    # city, longitude, latitude, name, category, tags
    __tablename__ = 'points_of_interest'
    id = db.Column(db.String(40), primary_key=True)
    city_name = db.Column(db.String(80), db.ForeignKey(
        'city.city_name'), nullable=False)
    longitude = db.Column(db.Float, nullable=True)
    latitude = db.Column(db.Float, nullable=True)
    attraction_name = db.Column(db.String(80), nullable=True)
    category = db.Column(db.String(100), nullable=True)
    tags = db.Column(db.String(500), nullable=True)
    img = db.Column(db.String(80), nullable=True)

    #city_id = db.Column(db.Integer, db.ForeignKey('city.id'))

    def serialize(self):
        return{
            city_name: self.city,
            longitude: self.longitude,
            latitude: self.latitude,
            attraction_name: self.name,
            category: self.category,
            tags: self.tags
        }

    """
    setup relationships with countries and cities
    """