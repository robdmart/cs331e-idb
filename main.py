from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy
from models import app, db, Country, City, Points_Of_Interest
import random

app = Flask(__name__)



persons = [{'img': './images/Thaddeus.png',
            'name': 'Thaddeus Dai',
            'bio': '3rd year Mechanical Engineering Major from Houston, TX',
            'responsibilities': 'Backend development, cities frontend',
            'commits': 6,
            'issues': 6,
            'unittests': 0,
            'tools': 'Flask, Bootstrap, HTML, CSS, Postgresql'},
           {'img': './images/Robert.jpg',
            'name': 'Robert Martinez',
            'bio': '3rd year Mathematics Major from Fort Worth, TX',
            'responsibilities': 'Project Leader, Frontend development, and API design', 
            'commits': 24,
            'issues': 10,
            'unittests': 4,
            'tools': 'Flask, Bootstrap, HTML, CSS, Python'},
           {'img': './images/Varun.jpg',
            'name': 'Varun Meduri',
            'bio': '3rd year Mechanical Engineering Major from Saratoga, CA',
            'responsibilities': 'Frontend for the countries page',
            'commits': 2,
            'issues': 1,
            'unittests': 5,
            'tools': 'Flask, Bootstrap, HTML, CSS'},
           {'img': './images/Max.jpg',
            'name': 'Maxwell Adu-Gyamfi',
            'bio': '4th year Computational Chemistry Major from Houston, TX',
            'responsibilities': 'Flask Backend, Postgres Database',
            'commits': 4,
            'issues': 7,
            'unittests': 0,
            'tools': 'Flask, HTML, Postgres'},
           {'img': './images/Patricia.jpg',
            'name': 'Patricia Cabanilla',
            'bio': '4th year Chemical Engineering Major from Katy, TX',
            'responsibilities': 'Frontend for the attractions page',
            'commits': 1,
            'issues': 1,
            'unittests': 0,
            'tools': 'Flask, Bootstrap, HTML, CSS'},
           {'img': './images/Nick.jpg',
            'name': 'Nick Martinez',
            'bio': '4th year Mechanical Engineering Major from Round Rock, TX',
            'responsibilities': 'Frontend for the About Us page, dynamic content display, and responsible for hosting on GCP',
            'commits': 6,
            'issues': 4,
            'unittests': 0,
            'tools': 'GCP, Flask, Bootstrap, HTML, CSS'}]

countries = db.session.query(Country).all()
cities = db.session.query(City).all()
attractions = db.session.query(Points_Of_Interest).all()


@app.route('/')
def splash_page():
    return render_template("splash2.html")


@ app.route('/countries/<string:sort_param>/<int:page>/', methods=['GET'])
def view_countries(sort_param, page):
    if sort_param == "currency":
        sorted_countries = sorted(
            countries, key=lambda item: item.currency)
    elif sort_param == "country_name":
        sorted_countries = sorted(
            countries, key=lambda item: item.country_name)
    elif sort_param == "country_code":
        sorted_countries = sorted(
            countries, key=lambda item: item.country_code)
    elif sort_param == "wiki":
        sorted_countries = sorted(
            countries, key=lambda item: item.wiki)
    else:
        sorted_countries = countries
    # sorted_countries = sorted(
    #     countries, key=lambda k: k[sort_param], reverse=reverse)
    # selected_countries = countries[(page-1)*8:(page)*8]
    return render_template("countries.html", countries=sorted_countries)


@app.route('/countries/<string:country>/cities/', methods=['GET'])
def country_link(country):
    city_list = []
    for city in cities:
        if country == city.country_name:
            city_list.append(city)
    return render_template("country_city.html", city_list=city_list, country=country)


@app.route('/country/<string:country>', methods=['GET'])
def country(country):
    selected = []
    for d in countries:
        if country == d.country_name:
            selected.append(d)
    return render_template("country.html", country=selected)


@ app.route('/cities/<string:sort_param>/<int:page>/', methods=['GET'])
def view_cities(sort_param, page):
    if sort_param == "city_name":
        sorted_cities = sorted(
            cities, key=lambda item: item.city_name)
    elif sort_param == "country_name":
        sorted_cities = sorted(
            cities, key=lambda item: item.country_name)
    elif sort_param == "longitude":
        sorted_cities = sorted(
            cities, key=lambda item: item.longitude)
    elif sort_param == "latitude":
        sorted_cities = sorted(
            cities, key=lambda item: item.latitude)
    elif sort_param == "region":
        sorted_cities = sorted(
            cities, key=lambda item: item.region)
    elif sort_param == "country_code":
        sorted_cities = sorted(
            cities, key=lambda item: item.country_code)
    else:
        sorted_cities = cities
    # sorted_cities = sorted(
    #     cities, key=lambda k: k[sort_param], reverse=reverse)
    # selected_cities = cities[(page-1)*8:(page)*8]
    return render_template("cities.html", cities=sorted_cities)


@app.route('/cities/<string:city>/attractions/', methods=['GET'])
def city_link(city):
    attraction_list = []
    for d in attractions:
        if city == d.city_name:
            attraction_list.append(d)
    return render_template("city_attraction.html", city=city, attractions_list=attraction_list)


@app.route('/city/<string:city>', methods=['GET'])
def city(city):
    selected = []
    for d in cities:
        if city == d['name']:
            selected.append(d.copy())
    return render_template("city.html", city='../JSON/cities.json')


@ app.route('/attractions/<string:sort_param>/<int:page>/', methods=['GET'])
def view_attractions(sort_param, page):
    if sort_param == "name":
        sorted_attractions = sorted(
            attractions, key=lambda item: item.attraction_name)
    elif sort_param == "city_name":
        sorted_attractions = sorted(
            attractions, key=lambda item: item.city_name)
    elif sort_param == "longitude":
        sorted_attractions = sorted(
            attractions, key=lambda item: item.longitude)
    elif sort_param == "latitude":
        sorted_attractions = sorted(
            attractions, key=lambda item: item.latitude)
    elif sort_param == "category":
        sorted_attractions = sorted(
            attractions, key=lambda item: item.category)
    else:
        sorted_attractions = attractions
    # sorted_attractions = sorted(
    #     attractions, key=lambda k: k[sort_param], reverse=reverse)
    # selected_attractions = attractions[(page-1)*8:(page)*8]
    return render_template("attractions.html", attractions=sorted_attractions)


@ app.route('/search', methods=["GET"])
def view_search():
    search_cities = []
    search_countries = []
    search_attractions = []
    if request.args.get("search"):
        search = request.args.get("search").title()
        search_cities = list(
            filter(lambda city: search in city.city_name, cities))
        search_countries = list(filter(
            lambda country: search in country.country_name, countries))
        search_attractions = list(filter(
            lambda attraction: search in attraction.attraction_name, attractions))
    return render_template(
        'search.html', cities=search_cities, countries=search_countries, attractions=search_attractions
    )


@ app.route('/about')
def view_about():
    return render_template(
        'about.html', persons=persons
    )


if __name__ == '__main__':
    app.debug = False
    app.run(host='0.0.0.0', port=8000)
